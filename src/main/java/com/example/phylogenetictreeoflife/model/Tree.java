package com.example.phylogenetictreeoflife.model;

import java.util.ArrayList;
import java.util.List;

public class Tree {
    private List<Node> nodes;

    public Tree() {
        this.nodes = new ArrayList<>();
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public void addNode(Node node) {
        nodes.add(node);
    }

    public static class Node {
        private Node parent;
        private int confidence, leaf, childCount, phylesis, hasPage, extinct, id;
        private String name;
        private List<Node> children = new ArrayList<>();
        private double x, y;

        public Node(int nodeId, String nodeName, int childNodes, int leafNode, int tolOrgLink, int extinct, int confidence, int phylesis) {
            this.id = nodeId;
            this.name = nodeName;
            this.childCount = childNodes;
            this.leaf = leafNode;
            this.hasPage = tolOrgLink;
            this.extinct = extinct;
            this.confidence = confidence;
            this.phylesis = phylesis;
            this.x = 0; // Position par défaut
            this.y = 0; // Position par défaut
        }

        public Node(int confidence, int leaf, int childCount, int phylesis, int hasPage, int extinct, int id, String name) {
            this.confidence = confidence;
            this.leaf = leaf;
            this.childCount = childCount;
            this.phylesis = phylesis;
            this.hasPage = hasPage;
            this.extinct = extinct;
            this.id = id;
            this.name = name;
        }


        public void addChild(Node child) {
            children.add(child);
            child.parent = this;
        }

        //Getters et setters
        public List<Node> getChildren() {
            return children;
        }
        public Node getParent() {
            return parent;
        }

        public int getConfidence() {
            return confidence;
        }

        public void setConfidence(int confidence) {
            this.confidence = confidence;
        }
        public int getLeaf() {
            return leaf;
        }
        public void setLeaf(int leaf) {
            this.leaf = leaf;
        }

        public int getChildCount() {
            return childCount;
        }

        public void setChildCount(int childCount) {
            this.childCount = childCount;
        }

        public int getPhylesis() {
            return phylesis;
        }

        public void setPhylesis(int phylesis) {
            this.phylesis = phylesis;
        }

        public int getHasPage() {
            return hasPage;
        }

        public void setHasPage(int hasPage) {
            this.hasPage = hasPage;
        }

        public int getExtinct() {
            return extinct;
        }

        public void setExtinct(int extinct) {
            this.extinct = extinct;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public double getX() {
            return x;
        }

        public void setX(double x) {
            this.x = x;
        }

        public double getY() {
            return y;
        }

        public void setY(double y) {
            this.y = y;
        }

    }
}
