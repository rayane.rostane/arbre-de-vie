package com.example.phylogenetictreeoflife;

import com.example.phylogenetictreeoflife.model.Tree;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InteractivePhylogeneticTreeApp extends Application {

    private double scaleValue = 1.0;
    private double zoomIntensity = 0.02;

    String nodesCsvFile = "";
    String linksCsvFile = "";

    List<Tree.Node> nodes;

    Tree tree;

    @Override
    public void start(Stage primaryStage) {
        Pane root = new Pane();
        root.setPrefSize(10000, 10000);
        Scene scene = new Scene(root, 1900, 1080);
        root.setStyle("-fx-background-color: black");
        scene.setFill(Color.BLACK);

        // accès aux fichiers csv
        nodesCsvFile = getClass().getResource("treeoflife_nodes.csv").getPath();
        linksCsvFile = getClass().getResource("treeoflife_links.csv").getPath();

        Task<List<Tree.Node>> treeTask = new Task<List<Tree.Node>>() {
            @Override
            protected List<Tree.Node> call() throws Exception {
                return readCSV(nodesCsvFile, linksCsvFile);
            }
        };

        treeTask.setOnSucceeded(event -> {
            nodes = treeTask.getValue();

            readLinksCSV(linksCsvFile, nodes, nodes.get(0).getId());

            tree = new Tree();
            tree.addNode(nodes.get(0));
            drawTree(root, tree.getNodes().get(0), 1900 / 2, 900, 200, 300);

        });


        treeTask.setOnFailed(event -> {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error");
            alert.setHeaderText("Failed to load data");
            alert.setContentText("An error occurred while loading the data.");
            alert.showAndWait();
        });

        Thread thread = new Thread(treeTask);
        thread.setDaemon(true);
        thread.start();

        // Activation "panning" et "zoom"
        setupZoomingAndPanning(root, scene);

        primaryStage.setTitle("Interactive Phylogenetic Tree");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setupZoomingAndPanning(Pane root, Scene scene) {
        final double[] initialTranslateX = {0};
        final double[] initialTranslateY = {0};
        final double[] anchorX = {0};
        final double[] anchorY = {0};

        scene.setOnMousePressed(event -> {
            // Enregistre les positions initiales quand la souris est enfoncée
            anchorX[0] = event.getSceneX();
            anchorY[0] = event.getSceneY();
            initialTranslateX[0] = root.getTranslateX();
            initialTranslateY[0] = root.getTranslateY();
            scene.setCursor(javafx.scene.Cursor.MOVE);
        });

        scene.setOnMouseDragged(event -> {
            // Calcule une nouvelle translation en fonction du décalage entre la position initiale et actuelle de la souris.
            root.setTranslateX(initialTranslateX[0] + event.getSceneX() - anchorX[0]);
            root.setTranslateY(initialTranslateY[0] + event.getSceneY() - anchorY[0]);
        });

        scene.setOnMouseReleased(event -> {
            scene.setCursor(javafx.scene.Cursor.DEFAULT);
        });

        // Zoom avec défilement
        scene.addEventFilter(ScrollEvent.SCROLL, event -> {
            double delta = 1.2;
            double scale = event.getDeltaY() > 0 ? delta : 1 / delta;
            zoom(root, scale, event.getSceneX(), event.getSceneY());
            event.consume();
        });

        // Double-clique pour zoom
        scene.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2) {
                double scale = 1.95;
                zoomAnim(root, scale, event.getSceneX(), event.getSceneY());
            }
        });
    }

    private void zoom(Pane root, double scaleFactor, double x, double y) {
        double oldScale = root.getScaleX();
        double newScale = oldScale * scaleFactor;

        // on limite l'echelle pour éviter un zoom (avant ou arrière) excessif
        newScale = Math.max(0.1, Math.min(newScale, 10.0));

        double f = (newScale / oldScale) - 1;
        double dx = (x - (root.getBoundsInParent().getWidth() / 2 + root.getBoundsInParent().getMinX()));
        double dy = (y - (root.getBoundsInParent().getHeight() / 2 + root.getBoundsInParent().getMinY()));

        root.setScaleX(newScale);
        root.setScaleY(newScale);

        // Ajuste la translation pour garder le zoom centré autour du curseur
        root.setTranslateX(root.getTranslateX() - f * dx);
        root.setTranslateY(root.getTranslateY() - f * dy);
    }

    private void zoomAnim(Pane root, double scaleFactor, double x, double y) {
        double oldScale = root.getScaleX();
        double newScale = oldScale * scaleFactor;

        // Limitation de l'échelle comme dans la méthode du zoom
        newScale = Math.max(0.1, Math.min(newScale, 10.0));

        double f = (newScale / oldScale) - 1;
        double dx = (x - (root.getBoundsInParent().getWidth() / 2 + root.getBoundsInParent().getMinX()));
        double dy = (y - (root.getBoundsInParent().getHeight() / 2 + root.getBoundsInParent().getMinY()));

        Timeline timeline = new Timeline(
                new KeyFrame(Duration.seconds(0.3),
                        new KeyValue(root.scaleXProperty(), newScale),
                        new KeyValue(root.scaleYProperty(), newScale),
                        new KeyValue(root.translateXProperty(), root.getTranslateX() - f * dx),
                        new KeyValue(root.translateYProperty(), root.getTranslateY() - f * dy)
                )
        );
        timeline.play();
    }





    private void drawTree(Pane root, Tree.Node node, double x, double y, double hGap, double vGap) {
        Text text = new Text(node.getName());
        text.setFont(Font.font("Verdana", 10));
        text.setFill(Color.WHITE);

        double radius = Math.max(text.getBoundsInLocal().getWidth(), text.getBoundsInLocal().getHeight()) / 2 + 5;
        Circle circle = new Circle(radius);
        LinearGradient gradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE,
                new Stop(0, Color.GREEN),
                new Stop(1, Color.DARKGREEN));
        circle.setFill(gradient);

        circle.setCenterX(x);
        circle.setCenterY(y);


        // Calcule la position des nœuds enfants au-dessus du nœud actuel
        double totalWidthNeeded = node.getChildren().size() * hGap;
        double childX = (x) - totalWidthNeeded / 2.0;
        double childY = (y) - vGap;

        if(node.getName().equals("Life on Earth")){

            Line line = new Line(x,900+10,x,1010);
            line.setStrokeWidth(5);
            line.setStroke(Color.WHITE);


            Line rootLine = new Line(x,1010,x+50,1040);
            rootLine.setStrokeWidth(5);
            rootLine.setStroke(Color.WHITE);


            Line rootLine2 = new Line(x,1010,x-50,1040);
            rootLine2.setStrokeWidth(5);
            rootLine2.setStroke(Color.WHITE);

            Line rootLine3 = new Line(x,1010,x,1040);
            rootLine3.setStrokeWidth(5);
            rootLine3.setStroke(Color.WHITE);

            root.getChildren().addAll(line,rootLine,rootLine2,rootLine3);

        }



        circle.setOnMouseEntered(e ->{
            Tree.Node n = getNode(text.getText());

            Tooltip tool=new Tooltip();
            if(node.getName().equals("Life on Earth")){
                tool.setText("Name: "+n.getName()+"\n" +
                        "Parent: Tree of Life\n" +
                        "Link: http://tolweb.org/"+n.getName()+"/"+n.getId()+"\n" +
                        "Extinct: "+n.getExtinct()+" ("+getExtinct(n.getExtinct())+") \n" +
                        "Confidence: "+n.getConfidence()+" ("+getConfidence(n.getConfidence())+") \n" +
                        "Phylesis: "+n.getPhylesis()+" ("+getPhylesis(n.getPhylesis())+") ");
            }else{
                tool.setText("Name: "+n.getName()+"\n" +
                        "Parent: "+n.getParent().getName()+"\n" +
                        "Link: http://tolweb.org/"+n.getName()+"/"+n.getId()+"\n" +
                        "Extinct: "+n.getExtinct()+" ("+getExtinct(n.getExtinct())+") \n" +
                        "Confidence: "+n.getConfidence()+" ("+getConfidence(n.getConfidence())+") \n" +
                        "Phylesis: "+n.getPhylesis()+" ("+getPhylesis(n.getPhylesis())+") ");
            }


            Tooltip.install(circle, tool);
        });


        text.setOnMouseEntered(e ->{
            Tree.Node n = getNode(text.getText());

            Tooltip tool=new Tooltip();
            if(node.getName().equals("Life on Earth")){
                tool.setText("Name: "+n.getName()+"\n" +
                        "Parent: Tree of Life\n" +
                        "Link: http://tolweb.org/"+n.getName()+"/"+n.getId()+"\n" +
                        "Extinct: "+n.getExtinct()+" ("+getExtinct(n.getExtinct())+") \n" +
                        "Confidence: "+n.getConfidence()+" ("+getConfidence(n.getConfidence())+") \n" +
                        "Phylesis: "+n.getPhylesis()+" ("+getPhylesis(n.getPhylesis())+") ");
            }else{
                tool.setText("Name: "+n.getName()+"\n" +
                        "Parent: "+n.getParent().getName()+"\n" +
                        "Link: http://tolweb.org/"+n.getName()+"/"+n.getId()+"\n" +
                        "Extinct: "+n.getExtinct()+" ("+getExtinct(n.getExtinct())+") \n" +
                        "Confidence: "+n.getConfidence()+" ("+getConfidence(n.getConfidence())+") \n" +
                        "Phylesis: "+n.getPhylesis()+" ("+getPhylesis(n.getPhylesis())+") ");
            }


            Tooltip.install(circle, tool);
        });

        circle.setOnMouseClicked(e ->{


            Tree.Node n = getNode(text.getText());

            clearSiblingChildren(n);
            n.getChildren().clear();
            readLinksCSV(linksCsvFile, nodes, getId(text.getText()));
            root.getChildren().clear();
            drawTree(root, nodes.get(0), 1900 / 2, 900, 200, 300);


        });

        text.setOnMouseClicked(e ->{

            Tree.Node n = getNode(text.getText());

            clearSiblingChildren(n);
            n.getChildren().clear();
            readLinksCSV(linksCsvFile, nodes, getId(text.getText()));
            root.getChildren().clear();
            drawTree(root, nodes.get(0), 1900 / 2, 900, 200, 300);
        });

        text.translateXProperty().bind(circle.centerXProperty().subtract(text.getBoundsInLocal().getWidth() / 2));
        text.translateYProperty().bind(circle.centerYProperty().add(text.getBoundsInLocal().getHeight() / 4));


        for (Tree.Node child : node.getChildren()) {
            Line line = new Line(x, y, childX, childY); // Draw line to child

            line.setStrokeWidth(5);

            line.setStrokeWidth(5);
            line.setStroke(Color.WHITE);
            root.getChildren().add(line);
            drawTree(root, child, childX, childY, hGap, vGap);  // Recursive call to draw each child
            childX += hGap;
        }

        root.getChildren().addAll(circle, text);
    }







    private Tree createSampleTree() {
        Tree tree = new Tree();

        List<Tree.Node> nodes = readCSV(nodesCsvFile, linksCsvFile);

        tree.addNode(nodes.get(0));

        return tree;
    }

    public static List<Tree.Node> readCSV(String nodesCsvFile, String linksCsvFile) {
        List<Tree.Node> nodes = new ArrayList<>();
        String line;
        String delimiter = ",";
        boolean skipHeader = true;
        Map<Integer, Tree.Node> nodeMap = new HashMap<>();

        try (BufferedReader br = new BufferedReader(new FileReader(nodesCsvFile))) {
            while ((line = br.readLine()) != null) {
                if (skipHeader) {
                    skipHeader = false;
                    continue;
                }
                String[] parts = line.split(delimiter);
                if (parts.length == 8) {
                    int nodeId = Integer.parseInt(parts[0]);
                    String nodeName = parts[1];
                    int childNodes = Integer.parseInt(parts[2]);
                    int leafNode = Integer.parseInt(parts[3]);
                    int tolOrgLink = Integer.parseInt(parts[4]);
                    int extinct = Integer.parseInt(parts[5]);
                    int confidence = Integer.parseInt(parts[6]);
                    int phylesis = Integer.parseInt(parts[7]);


                    Tree.Node node = new Tree.Node(nodeId, nodeName, childNodes, leafNode, tolOrgLink, extinct, confidence, phylesis);
                    nodes.add(node);
                    nodeMap.put(nodeId, node);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return nodes;
    }


    public static void readLinksCSV(String linksCsvFile, List<Tree.Node> nodes, int parentId) {
        String line;
        String delimiter = ",";
        Map<Integer, Tree.Node> nodeMap = new HashMap<>();

        for (Tree.Node node : nodes) {
            nodeMap.put(node.getId(), node);
        }

        boolean skipHeader = true;
        try (BufferedReader br = new BufferedReader(new FileReader(linksCsvFile))) {
            while ((line = br.readLine()) != null) {
                if (skipHeader) {
                    skipHeader = false;
                    continue;
                }
                String[] parts = line.split(delimiter);
                if (parts.length == 2) {
                    int sourceNodeId = Integer.parseInt(parts[0]);
                    int targetNodeId = Integer.parseInt(parts[1]);
                    if (sourceNodeId == parentId) {
                        Tree.Node parentNode = nodeMap.get(sourceNodeId);
                        Tree.Node childNode = nodeMap.get(targetNodeId);
                        if (parentNode != null && childNode != null) {
                            parentNode.addChild(childNode);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void clearNodeChildren(Tree.Node node){
        node.getChildren().clear();
    }




    private int  getId(String name){
        int id  = 0;
        for(Tree.Node node : nodes){
            if(node.getName().equals(name)){
                id = node.getId();
            }
        }
        return id;
    }

    private Tree.Node  getNode(String name){
        Tree.Node  node =  null;
        for(Tree.Node n : nodes){
            if(n.getName().equals(name)){
                node = n;
            }
        }
        return node ;
    }

    private void clearSiblingChildren(Tree.Node node) {
        List<Tree.Node> siblings = new ArrayList<>();

        if (node != null && node.getParent() != null) {
            siblings.addAll(node.getParent().getChildren());
            siblings.remove(node);
        }

        for(Tree.Node n : siblings){
            n.getChildren().clear();
        }

    }



    private String getConfidence(int n){
        String confidence  = "";


        if(n == 0){
            confidence = "Confident";
        }else if(n == 1){
            confidence = "Incertae sedis in putative position";
        }else{
            confidence = "Incertae sedis position unspecified (no putative position in parent group)";
        }

        return confidence;
    }

    private String getPhylesis(int n){
        String phylesis  = "";


        if(n == 0){
            phylesis = "Monophyletic";
        }else if(n == 1){
            phylesis = "Monophyletic";
        }else{
            phylesis = "Not Monophyletic";
        }

        return phylesis;
    }

    private String getExtinct(int n){
        String extinct  = "";
        if(n == 0){
            extinct = "Living";
        }else{
            extinct = "Extinct Species";
        }

        return extinct;
    }



    public static void main(String[] args) {
        launch(args);
    }
}