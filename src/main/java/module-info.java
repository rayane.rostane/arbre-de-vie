module com.example.phylogenetictreeoflife {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.phylogenetictreeoflife to javafx.fxml;
    exports com.example.phylogenetictreeoflife;
    exports com.example.phylogenetictreeoflife.model;
    opens com.example.phylogenetictreeoflife.model to javafx.fxml;
}